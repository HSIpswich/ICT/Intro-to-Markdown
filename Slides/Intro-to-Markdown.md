class: middle, center

<!-- Insert Logo here... -->
![](image\logo.png)


# Introduction to Markdown

## Presenter: Robert Manietta
### Cause Leader IT&T
### HSIpswich

<!-- Note: Housekeeping, emergency exits, toilets, amenaties. -->

---
class: middle, center

### What is Markdown?

Markdown is a lightweight markup of plain text files designed so that it can be converted into html and many other formats for publication.

John Gruber, the author of Markdown, puts it like this:

>The overriding design goal for Markdown’s formatting syntax is to make it as readable as possible. The idea is that a Markdown-formatted document should be publishable as-is, as plain text, without looking like it’s been marked up with tags or formatting instructions. While Markdown’s syntax has been influenced by several existing text-to-HTML filters, the single biggest source of inspiration for Markdown’s syntax is the format of plain text email. --  John Gruber

---
class: middle, left
### Why Learn Markdown?
</br>
  -  Markdown is simple to learn, with minimal extra characters so it's also quicker to write content.
  -  Less chance of errors when writing in markdown.
  -  Produces valid XHTML output.
  -  Keeps the content and the visual display separate, so you cannot mess up the look of your site.
  -  Write in any text editor or Markdown application you like.
  -  Markdown is a joy to use!

---
class: middle, left
### How do we start using Markdown
</br>
 - Markdown as described is a markup of plain text, so one needs a plain text editor
 - Online editing tools are available such as http://dillinger.io/
 - Web browsers have plugin editors
 - Another option is a Markdown editor that specifically provides a preveiw pane.

---

### Let's Start Writing
  - \#..... # { Headings like H1 thru H6 like in html}
  ```markdown
  # H1 Headings
  ## H2 Headings
  ### H3 Headings
  #### H4 Headings
  ##### H5 Headings
  ###### H6 Headings
  ```

  - \* text * {bold or strong} or single underscores
  ```Markdown
   * bold *
   _ bold _

   ** Italics **
   __ Italics __

   __* Bold and Italics *__
  ```

---

### Continuing to Write

  - Links \![Alt Text]\(url)  
  ```markdown
  [HSIpswich](http://www.hsipswich.org.au)
  ```

  - Images \![Alt Text]\(url)
  ```Markdown
  ![logo](image\logo.png)
  ```
  - Unordered lists
  ```markdown
    * Item
    * Item 2
    * Item 3
  ```

  - ordered lists

  ```Markdown
    1. Item
    2. Item
  ```   


---

### Continuing to Write

  - block quotes
  ```markdown
  > quotes
  ```

  - Escape charaters \* literal asterisks \*
  ```markdown
  \* literal asterisks \*
  ```

  - code blocks
  ```html
  <head><title> My blog </title></head>
  ```

---

### Continuing to Write

  - Task lists
  ```markdown
    - [] Item 1
    - [x] Next Item  
  ```

  - Some HTML elements may also be used.
  - Tables </br>

``` markdown
  Header 1 | Header 2
  --------| ----------
  Item 1 | Item 2
```

Header 1 | Header 2
---------|----------
Item 1 | Item 2

---
class: middle, center

## Assignment Time

HS Ipswich welcomes inderviduals to come by use our facilities to try and to practice these skills.
</br>
<hr>
</br>
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
