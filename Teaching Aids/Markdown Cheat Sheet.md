## HSIpswich ~ Introduction to Markdown Cheat Sheet.

### Headers
- \#..... # { Headings like H1 thru H6 like in html}

### Emphasis

- \_ text \_  - _undserscores emphasis_  
-  \*\* text \*\* - ** {bolder or stronger} **
- \__ text \__  - __{bold or stronger} __
- \__ \* text \*\__  - *__ {Can be mixed for stronger and emphasis} __*

### Links
- Links \[Alt Text]\(url) ie \[HSIpswich]\(http://hsipswich.org.au)
- [GOOGLE](https://google.com)

### Images
- Images \![Alt Text]\(url)

### Lists

- Unordered lists - \* Item \* Item 2 \* Item 3 ...
- ordered lists - 1. Item 2. Item 2 ...   

### Quotes

- block quotes > quotes

### Escaping Special Characters
- Escape charaters \\* literal asterisks

### Code Blocks
- code blocks ``` language code block ```  \``` language code block ```

### Task Lists

- Task lists - [] Item 1 - [x] Next Item
### Tables

- Tables </br>
| Header 1 | Header 2 |
| -------- | -------- |
| Item 1   | Item 2   |
|          |          |

\|Header 1 \| Header 2 \| <br/>
\|-------- \| ----------\| <br />
\|Item 1 \| Item 2 \|

### HTML

- HTML elements may also be used.

### Highlighting - Not always avaialbe, dependent on formatting.

- == text ==
<hr>
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
