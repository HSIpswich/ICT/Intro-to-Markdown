# Teaching Notes:

We will encourage the use of https://dillinger.io/ as a easy editor for beginers. 

Further to this we will export our slides to PDF for presentation and also do the same for the Assignment and the cheatsheet for our training course. 

It is also simple to change these slides to export as html slides with remark or other styling, as desired.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.