# Markdown Assignment

- Using markdown systax covered in the course and an editor of your choice.
- Create a new document "Assignment-fname-lname.md" eg. Assignment-Joe-Bloggs.md
- Please take your time and be as creative as you like in producing a document of your choice, using as many of the covered elements as possible.
- This document can contain anything, it could be a chapter of a novel your writing, documentation of a project, documentation of software, documentation of a Procedure, an article for a blog any written document really.

### Please present your assignment to the instructor for review.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.