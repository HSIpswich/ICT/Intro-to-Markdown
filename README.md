# Introduction to Markdown for documentation & Book Writing.

## HS Ipswich 2017


This course material covers an introdcution to Markdown for beginers to create, manage, and distribute digital documentation.

This can be used to document hardware, software and many other projects done as part of  business operations, and personally.


Commonly used in documentation of Open Source software projects, more companies are starting to use Markdown to document all aspects of their products, processes, and procedures as well as work flows.


Authors are discovering Markdown as a simple and elegant way to produce their digital editions and these editions can be easily exported to various formats for distribution through regular platforms.


When combined with a version control system such as GIT Markdown can become even more powerful and can have many authors on the same document. While maintaining control over the published content.


Markdown documentation can also be easily embeded into html websites and blogs making that work flow easier and faster for those doing this with a high frequency.


Wanting to learn more these resources may help, and you may never lose the bug to write quality documentation for your future projects.

### Note's:

Slides within this repository are written in markdown specific for making remark slides useing the remarkjs styling.

### Licencing

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

A copy of the full Licence is available in the repository in the file [LICENCE](https://gitlab.com/HSIpswich/Intro-to-Markdown/blob/master/LICENCE)

For further information on the licence please visit:  [Creative Commons Australia](http://creativecommons.org.au)
